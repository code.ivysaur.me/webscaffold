package main

import (
	"flag"
)

type Config struct {
	WebrootDir    string
	SchemaDir     string
	ListenAddress string
	Autocert      string
	DBPath        string
}

func main() {

	var cfg Config

	flag.StringVar(&cfg.WebrootDir, `WebrootDir`, "wwwroot/", "Webroot resource directory")
	flag.StringVar(&cfg.SchemaDir, `SchemaDir`, `schema/`, "SQL Schema resource directory")
	flag.StringVar(&cfg.ListenAddress, `ListenAddress`, "0.0.0.0:5454", "HTTP server network bind address")
	flag.StringVar(&cfg.Autocert, `Autocert`, "", "(Optional) Domain name to use for automatic HTTPS (leave blank for HTTP)")
	flag.StringVar(&cfg.DBPath, `DBPath`, AppName+`.db3`, "Path to SQLite3 database file")
	flag.Parse()

	app := Application{c: cfg}
	err := app.Start()
	if err != nil {
		panic(err)
	}
}
