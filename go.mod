module webscaffold

go 1.13

require (
	github.com/NYTimes/gziphandler v1.1.1
	github.com/friendsofgo/errors v0.9.2
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/pkg/errors v0.9.1
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0 // indirect
	github.com/spf13/viper v1.6.3
	github.com/volatiletech/inflect v0.0.0-20170731032912-e7201282ae8d // indirect
	github.com/volatiletech/null v8.0.0+incompatible
	github.com/volatiletech/sqlboiler v3.7.0+incompatible
	github.com/volatiletech/sqlboiler-sqlite3 v0.0.0-20180915213852-a153537eb0c3
	golang.org/x/crypto v0.0.0-20200420201142-3c4aac89819a
)
