package main

import (
	"log"
	"net/http"
)

type netError interface {
	Error() string
	Unwrap() error
	Respond(w http.ResponseWriter)
}

type userFacingError struct {
	code int
	e    error
}

func (ufe userFacingError) Error() string {
	return ufe.e.Error()
}

func (ufe userFacingError) Unwrap() error {
	return ufe.e
}

func (ufe userFacingError) Respond(w http.ResponseWriter) {
	w.Header().Set(`Content-Type`, `text/plain;charset=UTF-8`)
	if ufe.code != 0 {
		w.WriteHeader(ufe.code)
	} else {
		w.WriteHeader(400) // default
	}

	w.Write([]byte(ufe.e.Error()))
}

var _ netError = userFacingError{} // interface assertion

type systemError struct {
	e error
}

func (se systemError) Error() string {
	return se.e.Error()
}

func (se systemError) Unwrap() error {
	return se.e
}

func (se systemError) Respond(w http.ResponseWriter) {
	log.Printf(`[internal error] %s`, se.e.Error())

	w.Header().Set(`Content-Type`, `text/plain;charset=UTF-8`)
	w.WriteHeader(500)
	w.Write([]byte(`An internal error occurred.`))
}

var _ netError = systemError{} // interface assertion
