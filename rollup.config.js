// rollup.config.js
import typescript from '@rollup/plugin-typescript';
import {terser} from 'rollup-plugin-terser';
import less from 'rollup-plugin-less';
import LessPluginCleanCSS from 'less-plugin-clean-css';

export default {
  input: './webapp/js/app.ts',
  output: {
    dir: 'wwwroot/js/',
    format: 'cjs'
  },
  plugins: [
    typescript(),
    terser(),
    less({
      input: "./webapp/css/app.less",
      output: "wwwroot/css/app.css",
      option: {
        plugins: [new LessPluginCleanCSS({advanced: true})]
      }
    })
  ]
};
