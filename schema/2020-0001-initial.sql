-- 2020-0001. The initial schema
-- The Go SQLite driver does not support splitting by multiple statements
-- However, we simulate it by breaking on ENDBRACKET-SEMICOLON, so don't use that otherwise

CREATE TABLE `schema` (
	id INTEGER PRIMARY KEY NOT NULL
);

INSERT INTO `schema` (id) VALUES (20200001);
