-- 2020-0002. Initial user table
-- SQLite doesn't support UUID/BOOLEAN so just use TEXT/INTEGER

CREATE TABLE user (
	id              TEXT PRIMARY KEY NOT NULL,
	display_name    TEXT NOT NULL,
	email           TEXT NOT NULL,
	password_format INTEGER NOT NULL,
	password_hash   TEXT NOT NULL,
	is_admin        INTEGER NOT NULL
);

CREATE UNIQUE INDEX user_email ON user(email);

INSERT INTO user (id, display_name, email, password_format, password_hash, is_admin) VALUES
    ( "c3e6a5f2-3707-4799-a845-2dc9f51ebc31", "admin", "admin@example.com", 0, "admin", 1 );

INSERT INTO `schema` (id) VALUES (20200002);
