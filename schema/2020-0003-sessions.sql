-- 2020-0003. Sessions
-- An index is added for optimised time-based cleanup.
-- Session data could either be added directly as columns to this table, for type-safety; or as an untyped json data column

CREATE TABLE session (
	id      TEXT PRIMARY KEY NOT NULL,
	mtime   INTEGER NOT NULL,
	user_id TEXT NOT NULL
);

CREATE INDEX session_mtime ON session(mtime);

INSERT INTO `schema` (id) VALUES (20200003);
