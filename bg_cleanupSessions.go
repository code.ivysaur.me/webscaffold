package main

import (
	"log"
	"time"
)

const (
	sessionCleanupInterval = 30 * time.Minute
	sessionCleanupMaxAge   = 14 * 24 * time.Hour // stay logged in for 14 days of inactivity

)

func (this *Application) cleanupSessionsWorker() {
	for {

		cutOff := time.Now().Add(-sessionCleanupMaxAge).Unix()
		_, err := this.db.Exec(`DELETE FROM sessions WHERE mtime < ?`, cutOff)
		if err != nil {
			log.Printf(`cleaning up sessions: %s`, err.Error())
		}

		// Wait for next ticker
		time.Sleep(sessionCleanupInterval)

	}
}
