// MUTABLE STATE

import { APIClient } from "./API";

class State {

	api: APIClient;

	constructor() {
		this.api = new APIClient();
	}

	isLoggedIn(): boolean {
		return this.api.sessionKey != ""
	}

}

let s = new State();
export default s;
