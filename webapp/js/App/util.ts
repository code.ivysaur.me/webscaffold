
/**
 * HTML escape
 * 
 * @param {string} sz
 * @return {string}
 */
export function hesc(sz: string): string {
	return sz
		.replace(/&/g,'&amp;')
		.replace(/</g,'&lt;')
		.replace(/>/g,'&gt;')
		.replace(/"/g,'&quot;')
		.replace(/'/g,'&#39;');
}

/**
 * html is a no-op function for ES6 literals.
 * Install the `lit-html` extension in VSCode to use this as a syntax-highlighted string literal.
 * 
 * @param strings {TemplateStringsArray}
 * @param keys {string[]}
 */
export function html(strings: TemplateStringsArray, ...keys: string[]): string {
	let ret = [];
	for (let i = 0; i < strings.length - 1; ++i) {
		ret.push(strings[i], keys[i]);
	}
	ret.push(strings[strings.length - 1]);
	return ret.join("");
};

/**
 * toast shows a mini temporary informational popup, using bootstrap's toast library.
 * 
 * @param titleHtml {string}
 * @param bodyHtml {string}
 * @param colorClass {string} One of 'info', 'success', 'danger'
 */
export function toast(titleHtml: string, bodyHtml: string, colorClass: string = "info") {

	const template = html`
<div class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="true" data-delay="5000">
  <div class="toast-header bg-` + colorClass + ` text-white">
	<strong class="mr-auto">` + hesc(titleHtml) + html`</strong>
	<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
	  <span aria-hidden="true">&times;</span>
	</button>
  </div>
  <div class="toast-body">` + hesc(bodyHtml) + html`</div>
</div>`;

	let $t = $(template);
	$(".toast-container").append($t);
	$t.toast('show');
	$t.on('hidden.bs.toast', function () {
		$t.remove();
	})

}
