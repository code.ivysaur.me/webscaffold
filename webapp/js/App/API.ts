
export class ApiLoginResponse {
	SessionKey: string = ""
}

export class APIClient {

	sessionKey: string = ""

	constructor() {}

	async Login(email: string, password: string): Promise<ApiLoginResponse> {
		const postURL = window.location.protocol + "//" + window.location.host + "/api/v1/login";
		const postData = {"email": email, "password": password};
		
		let postResult = await $.post(postURL, $.param(postData)).promise();

		return postResult as ApiLoginResponse; // hope the server/client types remain in sync
	}

}
