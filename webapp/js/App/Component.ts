
export default abstract class Component {

	$area: JQuery<HTMLElement>

	constructor() {
		this.$area = $("<div>");
	}

	mountInto($parent: JQuery<HTMLElement>): void {
		$parent.html('');
		$parent.append(this.$area);
	}

}
