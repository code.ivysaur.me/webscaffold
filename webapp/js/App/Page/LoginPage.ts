import Component from "../Component";
import state from "../state";
import { html, toast, hesc } from "../util";

export default class LoginPage extends Component {
	
	constructor() {
		super();

		this.$area.html(html`
<div class="row justify-content-center">
	<div class="col-4 card">
		<form class="form-group app-login card-body">
			<h2>Log in</h2>
			<input class="form-control login-email" type="email" placeholder="Email"><br>
			<input class="form-control login-passwd" type="password" placeholder="Password"><br>
			<input type="submit" class="btn btn-primary" value="Log in">
		</form>
	</div>
</div>
		`);

		this.$area.find(".app-login").on('submit', (ev) => this.doLogin(ev));
	}

	async doLogin(ev: JQuery.SubmitEvent) {
		ev.preventDefault();

		let email = this.$area.find(".login-email").val() as string;
		let passwd = this.$area.find(".login-passwd").val() as string;

		try {
			let resp = await state.api.Login(email, passwd);
			this.$area.html(JSON.stringify(resp));

			// Stash our successful login state
			state.api.sessionKey = resp.SessionKey

			// Navigate to homepage
			window.location.hash = "/" // will take us to the homepage now

			toast("Logged in", "", "success")

		} catch (ex) {
			// network error or some other kind of problem
			if (("responseText" in ex) && ex.responseText.length > 0) {
				toast(hesc("Login failed"), hesc(ex.responseText), "danger");

			} else {
				toast(hesc("Login failed"), hesc("An unknown error occurred.\n\n" + JSON.stringify(ex)), "danger");
			}
			
		}

		return false; // like preventDefault()
	}

}
