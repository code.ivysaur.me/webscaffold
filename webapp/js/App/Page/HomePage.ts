import Component from "../Component";
import state from "../state";

export default class HomePage extends Component {
	
	constructor() {
		super();

		this.$area.html("home page (logged in with session key " + state.api.sessionKey + ")");
	}

}
