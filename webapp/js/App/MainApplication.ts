import state from "./state.js";
import Component from "./Component.js";
import LoginPage from "./Page/LoginPage.js";
import HomePage from "./Page/HomePage.js";
import { html, toast, hesc } from "./util.js";

export default class MainApplication extends Component {

	constructor() {
		super();

		$("title").text('Application');

		this.$area.html(html`
<div class="toast-container-outer"><div class="toast-container"></div></div>

<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
	<h5 class="my-0 mr-md-auto font-weight-normal">Application</h5>
	<nav class="my-2 my-md-0 mr-md-3">
		<a class="p-2 text-dark" href="#">Features</a>
		<a class="p-2 text-dark" href="#">Enterprise</a>
		<a class="p-2 text-dark" href="#">Support</a>
		<a class="p-2 text-dark" href="#">Pricing</a>
	</nav>
	<a class="btn btn-outline-primary" href="#">Sign up</a>
</div>

<main class="component-page container"></main>

<footer class="footer">
	<div class="container">
		<span class="text-muted">Copyright &copy; 2020</span>
	</div>
</footer>
		`);
	}

	mountInto($parent: JQuery<HTMLElement>): void {
		super.mountInto($parent);

		this.routeToHash();
		window.addEventListener('hashchange', (ev) => this.routeToHash(), false);
	}

	routeToHash() {

		switch (window.location.hash.substr(1)) {
			case "/": {
				// Redirect to "home page" based on our current state
				if (state.isLoggedIn()) {
					window.location.hash = `/home`
				} else {
					window.location.hash = `/login`
				}
			} break;

			case "/home": {
				if (!state.isLoggedIn()) {
					toast(hesc("Unauthorised"), hesc("Please log in."), "danger");
					window.location.hash = `/`;
					return;
				}

				let p = new HomePage();
				this.renderPage(p);                
			} break;

			case "/login": {
				if (state.isLoggedIn()) {
					window.location.hash = `/`;
					return;
				}

				let p = new LoginPage();
				this.renderPage(p);
			} break;

			default: {
				// Redirect to /
				window.location.hash = `/`
			} break;
		}
	}

	renderPage(p: Component): void {
		p.mountInto(this.$area.find(".component-page"));
	}

}
