import MainApplication from './App/MainApplication.js';

import '../css/app.less'; // hit the rollup.js loader

$(function() {
	let app = new MainApplication();
	app.mountInto( $("body") );
});
