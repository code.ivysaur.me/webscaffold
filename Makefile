
.PHONY: models
models:
	${GOPATH}/bin/sqlboiler ${GOPATH}/bin/sqlboiler-sqlite3

.PHONY: deps
deps:
	go get
	npm ci # use package versions from package.lock only 
