# Production Dockerfile
# Not necessary for development

# Minify JS
FROM node:10-alpine AS nodebuilder
WORKDIR /app
COPY . /app/
RUN \
    npm ci && \
    npm run build

# Compile Go binary
FROM golang:1.14-alpine AS gobuilder
WORKDIR /app
COPY . /app/
RUN apk --no-cache add gcc libc-dev
RUN go build -ldflags "-s -w"

# Minimal runtime container
FROM alpine:latest
WORKDIR /app
COPY --from=nodebuilder /app/wwwroot /app/wwwroot
COPY --from=gobuilder /app/webscaffold /app/webscaffold
COPY ./schema /app/schema

CMD ["./webscaffold"]
