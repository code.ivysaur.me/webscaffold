# webscaffold

![](https://img.shields.io/badge/license-BSD0-informational)

`webscaffold` is a sample web application project that can be adopted as a base.

This project is made available to you under the terms of the [0-clause BSD license](LICENSE) i.e. you may take this code without attribution.

## Development

- Install node.js + Go dependencies: `make deps` *(gitignored)*
- Rebuild SQL model layer: `make models` *(should commit)*
- Live watch reload: `npm run watch` *(gitignored)*
- Compile backend: `go build`
- Run development instance: `./webscaffold`
    - This will use the live wwwroot directory, that is managed by the live watch reload
- Run production instance: `dokku apps:create webscaffold`

## Features

- Go backend
    - [X] Integrated HTTP server
        - [X] HTTP/2 support
        - [X] Optional Let's Encrypt integration for HTTPS
        - [X] Gzip of API responses
    - [X] Exact match router
    - [X] SQLite database
        - [X] Migrations support
            - [X] `schema` table for tracking migrations
            - [X] Automatically apply new migrations at daemon startup    
        - [X] SQLBoiler for strict types
    - [X] Basic admin/user login data types ready implemented
        - [X] Password hashing with `argon2i`
    - [X] Session support
        - [X] DB persistence for sessions
        - [X] Automatic session cleanup
        - [ ] Authentication helpers for validating session data
- SPA frontend
    - [X] Typescript
        - [X] Object-oriented class design
        - [X] jQuery for DOM access and ajax, using async promises
        - [X] CoreJS polyfill for IE browsers
        - [X] Hash fragment routing
        - [X] `var state;`
        - [X] Toast popups
    - [X] LESS CSS
        - [X] Bootstrap 4.x template integrated
        - [X] Responsive mobile friendly
        - [X] LESS CSS compilation working within the rollup watch script
    - [X] Secure headers
        - [X] Strict Content-Security-Policy (CSP)
        - [-] Subresource integrity (SRI)
            - [X] for vendor scripts
                - Regenerate via `cat FILE | openssl dgst -sha384 -binary | openssl base64 -A`
            - [ ] for first-party scripts
        - [X] X-Frame-Options deny
- Docker for production
    - [X] Multi-stage docker build
    - [X] Dokku deployment steps
- Makefile for all scripts
    - [X] Watch-based minification
    - [ ] Single command to reset base app name / branding
